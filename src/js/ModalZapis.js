import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Portal from './Portal';
import Icon from './Icon';
import ModalForm from './ModalForm';
import Signup from './Signup';
import Modal from './Modal';
import ModalOk from './ModalOk';
import SendMail from './SendMailJS';
import Header from './Header';

// import Button from './Button';

import '../css/modal.css';

function ModalZapis(props) {


  const  handleSubmit = (data) => {
    console.log(data);
    var sendmail = new SendMail(data[0], data[1], data[2]);
    sendmail.sendFeedback();
    props.onCancel();
    props.onSubmit();
  }

  return (
    <>
        <Portal>
        <Modal
          title="Первый раз у нас ? Оставь заявку сейчас и получи скидку 30%"
          isOpen={true}
          onCancel={props.onCancel}
          onSubmit={handleSubmit}
        />

        </Portal>
    </>
  );
};

ModalZapis.propTypes = {
  title: PropTypes.string,
  isOpen: PropTypes.bool,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  children: PropTypes.node,
};

ModalZapis.defaultProps = {
  title: 'Modal title',
  isOpen: true,
  onCancel: () => {},
  onSubmit: () => {},
  children: null,
};

export default ModalZapis
