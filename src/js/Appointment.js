import React from 'react'

// import 'bootstrap/dist/css/bootstrap.min.css';
import ModalOk from './ModalOk';
import sectionImg from '../images/girl-banner.png'

// var textStyle = { textAlign: 'left', color: '#ffffff', marginTop: '10px', fontSize: '10pt' };

class Appointment extends React.Component{
    city = '';
    name = '';
    phone = '';

    state = {
      isOpen: false,
    };
    openModal = () => {
      this.setState({ isOpen: true });
    };
    handleCancel = () => {
      this.setState({ isOpen: false });
    }

    onChangeCity = (event) => {
        this.city = event.currentTarget.value;
    };

    onChangeName = (event) => {
        this.name = event.currentTarget.value;
    };

    onChangePhone = (event) => {
        this.phone = event.currentTarget.value;
    };

    onSubmit = (event) => {
        event.preventDefault();
        this.props.func([this.name, this.phone, this.city])
    };

    render(){
        return (

          <section className="banner-wrapper wrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12 col-md-6">
                            <div className="text-banner">
                                <div className="title-banner">
                                    Оставь заявку и получи скидку 30%
                                </div>
                                <div className="title-banner__item">
                                    *При первом посещении
                                </div>
                                {
                                    // <p style={textStyle}><em>* Скидка действует только на Томск и на Барнаул</em></p>
                                }
                            </div>
                        </div>
                        <div className="col-xs-12 col-md-6">
                            <div className="banner-form">
                                <img className="girl-form" src={sectionImg}/>
                                <div className="banner-form-container">
                                    <form name="AppForm" onSubmit={this.onSubmit}>
                                        <input type="text" placeholder="Имя" name="name" onChange={this.onChangeName}/>
                                        <input type="tel" placeholder="Телефон" name="phone" onChange={this.onChangePhone}/>
                                        <div className="select-container">
                                            <select name="city" onChange={this.onChangeCity}>
                                                <option disabled="" select="">Выберите город</option>
                                                <option value="Томск">Томск</option>
                                                <option value="Барнаул">Барнаул</option>
                                                <option value="Кемерово">Кемерово</option>
                                            </select>
                                        </div>
                                        <label>
                                        <button onClick={this.openModal} type="submit" id="submit-banner">
                                            Записаться
                                        </button>
                                        <ModalOk
                                          title="Спасибо,ваша заявка принята!"
                                          func={this.handleSubmit}
                                          isOpen={this.state.isOpen}
                                          onCancel={this.handleCancel}
                                          onSubmit={this.handleSubmit}
                                        >

                                        </ModalOk>
                                        </label>
                                        {
                                            // <div className="submit-banner-button">
                                            //   <button onClick={(data) => func([AppForm.name.value, AppForm.phone.value, AppForm.city.value])}>Записаться</button>
                                            // </div>
                                        }

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Appointment
