import React, { useRef, useState, useEffect } from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MenuIcon from "@material-ui/icons/Menu";
import IconButton from "@material-ui/core/IconButton";
import SideDrawer from "./SideDrawer";
import Modal from './Modal';
import ModalOk from './ModalOk';
import ModalZapis from './ModalZapis';
import Signup from './Signup';
import SendMail from './SendMailJS';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import purple from '@material-ui/core/colors/purple';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  root: {
    'position': 'absolute',
  },
}));



const theme = createMuiTheme({
  palette: {
    primary: purple,
  },
});

const Header = (props) => {
  const [modalOkOpen, setmodalOkOpen] = useState(false);
  const [signupOpen, setSignupOpen] = useState(false);
  const [drawerOpen, setDrawerOpen] = useState(false);
  const [headerShow, setHeaderShow] = useState(false);

  const closeModalOk = () => {
    setmodalOkOpen(false);
  };
  const toggleDrawer = value => {
    setDrawerOpen(value);
    console.log(value)
  };
  const toggleSignupOpen = value => {
    setSignupOpen(value);
  };
  const handleClose = () => {
    setSignupOpen(false);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  }, [headerShow]);
  const handleScroll = () =>{
    (window.scrollY > 0) ? setHeaderShow(true) : setHeaderShow(false);
  }

  const classes = useStyles(props);
  return (
    <AppBar
      style={{
        background: headerShow ? "lightgray" : "white",
        color: "#000000",
        padding: "5px 0px"
      }}
    >
      <Toolbar>
        <IconButton className={classes.root} aria-label="Menu" color="inherit" onClick={() => toggleDrawer(true)}>
          <MenuIcon />
        </IconButton>
        <Box mx='auto'>
          <ThemeProvider theme={theme}>
          <Button onClick={() => toggleSignupOpen(true)} variant="contained" color="primary">Записаться</Button>
          </ThemeProvider>
        </Box>
        {signupOpen && <ModalZapis onCancel={handleClose} onSubmit={()=> setmodalOkOpen(true)} />}
        <SideDrawer open={drawerOpen} onClose={value => toggleDrawer(value)} />
        <ModalOk
          title="Спасибо,ваша заявка принята!"
          isOpen={modalOkOpen}
          onCancel = {closeModalOk}
        >
        </ModalOk>
      </Toolbar>
    </AppBar>

  );
};

export default Header;
