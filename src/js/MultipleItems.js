import React, { Component } from "react";
import Slider from "react-slick";
import oneSlide from '../images/otzivi/1.jpg'
import twoSlide from '../images/otzivi/2.jpg'
import threeSlide from '../images/otzivi/3.jpg'
import fourSlide from '../images/otzivi/4.jpg'
import fiveSlide from '../images/otzivi/5.png'
import sixSlide from '../images/otzivi/6.jpg'
import sevenSlide from '../images/otzivi/7.png'
import eightSlide from '../images/otzivi/8.jpg'
import nineSlide from '../images/otzivi/9.jpg'
import tenSlide from '../images/otzivi/10.png'
import elevenSlide from '../images/otzivi/11.png'
import twelveSlide from '../images/otzivi/12.jpg'
import treteenSlide from '../images/otzivi/13.jpg'
import fourteenSlide from '../images/otzivi/14.jpg'
import fiveteenSlide from '../images/otzivi/15.jpg'
import sixteenSlide from '../images/otzivi/16.png'
import seventeenSlide from '../images/otzivi/17.png'
import eightteenSlide from '../images/otzivi/18.png'
import nineteenSlide from '../images/otzivi/19.png'
import twentySlide from '../images/otzivi/20.png'
import twentyoneSlide from '../images/otzivi/21.png'
import twentytwoSlide from '../images/otzivi/22.png'
import twentythreeSlide from '../images/otzivi/23.png'
import twentyfourSlide from '../images/otzivi/24.jpg'

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


class SimpleSlider extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 2,
      slidesToScroll: 1,
      adaptiveHeight: true,
      autoplay: true,
      className: 'mySimpleSlider',
      centerMode: true,
    };
    return (

        <Slider {...settings}>
          <div>
            <h3> <img src={oneSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={twoSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={threeSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={fourSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={fiveSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={sixSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={sevenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={eightSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={nineSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={tenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={elevenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={twelveSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={treteenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={fourteenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={fiveteenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={sixteenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={seventeenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={eightteenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={nineteenSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={twentySlide} /> </h3>
          </div>
          <div>
            <h3> <img src={twentyoneSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={twentytwoSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={twentythreeSlide} /> </h3>
          </div>
          <div>
            <h3> <img src={twentyfourSlide} /> </h3>
          </div>
        </Slider>

    );
  }
}
export default SimpleSlider
