import React from 'react';
import PropTypes from 'prop-types';
import Portal from './Portal';
import Icon from './Icon';

// import Button from './Button';

import '../css/modal.css';

const ModalOk = ({
    title, isOpen, onCancel, onSubmit, children, func
}) => {

  return (
    <>
      { isOpen &&
        <Portal>
          <div className="modalOverlay">
            <div className="modalWindow">
              <div className="close-modal" onClick={onCancel}>X</div>
              <div className="modalHeader">
                <div className="modalTitle">{title}</div>
              </div>
              <div className="modalBody">
                {children}
              </div>
              <div className="modalFooter">
                <div>
                  <ul>
                    <li>Мы свяжемся с вами в ближайшее время</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </Portal>
      }
    </>
  );
};

ModalOk.propTypes = {
  title: PropTypes.string,
  isOpen: PropTypes.bool,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  children: PropTypes.node,
};

ModalOk.defaultProps = {
  title: 'Modal title',
  isOpen: true,
  onCancel: () => {},
  onSubmit: () => {},
  children: null,
};

export default ModalOk
