import React from 'react';
import PropTypes from 'prop-types';
import ModalForm from './ModalForm'
import Portal from './Portal';
import Icon from './Icon';
// import Button from './Button';

import '../css/modal.css';

const Modal = ({
    title, isOpen, onCancel, onSubmit, children, func
}) => {

  return (
    <>
      { isOpen &&
        <Portal>
          <div className="modalOverlay">
            <div className="modalWindow">
              <div className="close-modal" onClick={onCancel}>X</div>
              <div className="modalHeader">
                <div className="modalTitle">{title}</div>
              </div>
              <div className="modalBody">
                <ModalForm onSubmit={onSubmit} />
              </div>

            </div>
          </div>
        </Portal>
      }
    </>
  );
};

Modal.propTypes = {
  title: PropTypes.string,
  isOpen: PropTypes.bool,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func,
  children: PropTypes.node,
};

Modal.defaultProps = {
  title: 'Modal title',
  isOpen: true,
  onCancel: () => {},
  onSubmit: () => {},
  children: null,
};

export default Modal
