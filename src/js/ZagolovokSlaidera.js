import React from 'react'
import sectionImg from '../images/iblock/451/table_header.png'

class ZagolovokSlaidera extends React.Component {
  render () {
    return (
      <section className="service-price-wrapper wrapper">
      	<div className="container">
      		<div className="title-wrapper">
      			<div className="title-wrapper__inner">
      				<div className="title-wrapper__header">
      					<span>
      						<img src={sectionImg} alt="Прайсы" />
      					</span>
      				</div>
      				<div className="title-wrapper__title">
      					Отзывы
              </div>
      				<div className="title-wrapper__footer">
      					<span>Наших клиентов</span>
      				</div>
      			</div>
      		</div>
      	</div>
      </section>
    )
  }
}

export default ZagolovokSlaidera
